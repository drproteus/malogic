import os, shutil
from config import LOGIC_FUNCTION_BASEPATH, LOGIC_FUNCTION_MODULEBASE
from app import models, db
from backup_logic_functions import BACKUP_BASEPATH

def get_choices():
    choices = [path for path in os.listdir(BACKUP_BASEPATH) if "logic_functions_" in path]
    return choices

def main():
    backup_choices = get_choices()
    if not backup_choices:
        print "No backups found. Aborting..."
        return
    print "=============================="
    print "Select backup to restore from:"
    print "=============================="
    for i, choice in enumerate(backup_choices):
        print i, choice 
    choice = input('> ')
    if type(choice) != int:
        print "Bad input."
        return
    if choice not in range(len(backup_choices)):
        print "Selection not in range."
        return
    print "Selected: %s" % (backup_choices[choice])
    print "Warning: This will invalidate all false positives marked in the DB."
    print "Press enter to continue, Ctrl-C to abort."
    try:
        raw_input('> ')
    except KeyboardInterrupt:
        return
    [db.session.delete(l) for l in models.LogicCheck.query.all()]
    db.session.commit()
    shutil.rmtree(LOGIC_FUNCTION_BASEPATH, ignore_errors=True)
    shutil.copytree(os.path.join(BACKUP_BASEPATH, backup_choices[choice]), LOGIC_FUNCTION_BASEPATH)
    with open(os.path.join(LOGIC_FUNCTION_BASEPATH, 'info')) as f:
        for line in f.readlines():
            name, function_name, function_path, function_module, field_dependencies, error_message = line.strip().split('$')
            new_logic = models.LogicCheck(name=name, function_name=function_name, function_module=function_module, function_path=function_path, field_dependencies=field_dependencies, error_message=error_message)
            db.session.add(new_logic)
    [db.session.delete(fp) for fp in models.FalsePositive.query.all()]
    db.session.commit()
    print "Restored %s" % (backup_choices[choice])

if __name__ == "__main__":
    main()
    
