# README #

### What is this repository for? ###

* Mergers & Acquisitions Logic checker. Author validation checks in Python to run against data queries.

### SCREENSHOTS ###

* Verification Screen
![alt text](http://i.imgur.com/5Tp24ry.png "Verification Screen")
* Editor Screen
![alt text](http://i.imgur.com/9QVNphP.png "Editor Screen")
* List of Checks
![alt text](http://i.imgur.com/J3OJ2Jr.png "Registered Checks")

### How do I get set up? ###

* TODO

### Who do I talk to? ###

* jgoritski@bloomberg.net