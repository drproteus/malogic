# SimpleRefDataExample.py

import blpapi
import pandas as pd
from optparse import OptionParser

SECURITY_DATA = blpapi.Name("securityData")
SECURITY = blpapi.Name("security")
FIELD_DATA = blpapi.Name("fieldData")
FIELD_EXCEPTIONS = blpapi.Name("fieldExceptions")
FIELD_ID = blpapi.Name("fieldId")
ERROR_INFO = blpapi.Name("errorInfo")
COOKIE = blpapi.Name("sequenceNumber")

def parseCmdLine():
    parser = OptionParser(description="Retrieve reference data.")
    parser.add_option("-a",
                      "--ip",
                      dest="host",
                      help="server name or IP (default: %default)",
                      metavar="ipAddress",
                      default="localhost")#"nytechops03"
    parser.add_option("-p",
                      dest="port",
                      type="int",
                      help="server port (default: %default)",
                      metavar="tcpPort",
                      default=8194)

    (options, args) = parser.parse_args()

    return options

def main(tickerList,fieldList):
    options = parseCmdLine()
    sessionOptions = blpapi.SessionOptions()
    sessionOptions.setServerHost(options.host)
    sessionOptions.setServerPort(options.port)
    session = blpapi.Session(sessionOptions)
    session.start()
    session.openService("//blp/refdata")
    refDataService = session.getService("//blp/refdata")
    request = refDataService.createRequest("ReferenceDataRequest")

    resultList = []
    for tkr in tickerList:
        request.getElement("securities").appendValue(tkr)

    for fld in fieldList:
        request.getElement("fields").appendValue(fld)

    session.sendRequest(request)

    try:
        while(True):
            ev = session.nextEvent(500)
            if ev.eventType() == blpapi.Event.PARTIAL_RESPONSE or ev.eventType() == blpapi.Event.RESPONSE:
                for msg in ev:
                    securityDataArray = msg.getElement(SECURITY_DATA)
                    for securityData in securityDataArray.values():
                        fieldData = securityData.getElement(FIELD_DATA)
                        resultList.append([securityData.getElementAsString(SECURITY),[''] * len(fieldList),
                            securityData.getElementAsString(COOKIE)])
                        for field in fieldData.elements():
                            if field.isArray():
                                bulkList = []
                                for each in field.values():
                                    bulkList2 = []
                                    bulkList.append(bulkList2)
                                    for i in each.elements():
                                        bulkList[len(bulkList)-1].append(i.getValueAsString())
                                if len(bulkList) == 1 and len(bulkList[0]) == 1:
                                    resultList[len(resultList)-1][1][getFieldIndex(fieldList,field.name())] = str(bulkList[0][0])
                                else:
                                    resultList[len(resultList)-1][1][getFieldIndex(fieldList,field.name())] = bulkList
                            elif field.isNull():
                                pass
                            elif field.isComplexType():
                                pass
                            else:
                                resultList[len(resultList)-1][1][getFieldIndex(fieldList,field.name())] = field.getValueAsString()

            if ev.eventType() == blpapi.Event.RESPONSE:
                break

    finally:
        session.stop()

    #print resultList
    return resultList

def getFieldIndex(fieldList,fieldName):
    for i in range(len(fieldList)):
        if fieldList[i] == str(fieldName).split(' ')[0]:
            return i
        

