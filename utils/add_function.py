# -*- coding: utf-8 -*-
"""
@author: jgoritski
"""
import os
basedir = os.path.abspath(os.path.dirname(__file__))

FUNCTION_FILE = os.path.join(basedir, "logic_checks.py")
BEGIN = "# BEGIN LOGIC FUNCTIONS"
END = "# END LOGIC FUNCTIONS"
ARRAY_END = "] # END LOGIC ARRAY"

TEST_FUNC = """def check_em(ca_df):
    return"""

def add_function(body):
    body_lines = body.split('\n')
    if not (body_lines[0].startswith("def") and body_lines[0].endswith("(ca_df):")):
        raise Exception, "Malformed function."
    function_name = body_lines[0].split(" ")[1].split('(')[0]
    with open(FUNCTION_FILE, 'r') as f:
        lines = map(lambda line: line[:-1], f.readlines())
    END_INDEX = lines.index(END)
    for i, body_line in enumerate((body+'\n').split('\n')):
        lines.insert(END_INDEX+i, body_line)
    ARRAY_END_INDEX = lines.index(ARRAY_END)
    lines.insert(ARRAY_END_INDEX, (' '*10)+function_name+',')
    new_lines = "\n".join(lines)
    with open(FUNCTION_FILE, 'w') as f:
        f.write(new_lines)
