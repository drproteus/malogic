import pandas as pd


def ensure_completion_after_announcement_date(ca_df):
    return ca_df.CA057 <= ca_df.CA058

def ensure_target_name_present(ca_df):
    return pd.notnull(ca_df.CA051)

def always_false(ca_df):
    return pd.Series(False, index=ca_df.index)
