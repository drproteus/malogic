# -*- coding: utf-8 -*-
"""
@author: jgoritski
"""
import pandas as pd
import ServerApi
from constants import *


def _fetch(items, fields, ignore_empty=False):
    if ignore_empty:
        return [x for x in ServerApi.main(items, fields) if reduce(lambda u,v: u+v, x[1])]
    return ServerApi.main(items, fields)


def get_data(items, fields=TEST_FIELDS, ignore_empty=False):
    '''
    Return a pandas DataFrame indexed on the items with given fields
    '''

    # Append the ' Action' suffix if necessary
    item_suffix = ' Action'
    if type(items) != list:
        items = [items]
    items = [str(item) for item in items]
    for i in range(len(items)):
        if not items[i].endswith(item_suffix):
            items[i] += item_suffix

    # Grab the raw data from the ServerApi
    data = _fetch(items, fields, ignore_empty=ignore_empty)

    # Remove indices from data
    index = [datum.pop(0) for datum in data]

    # Isolate field values
    data = [datum[:1][0] for datum in data]
    
    # Construct and return the DataFrame
    return pd.DataFrame(data=data, index=index, columns=fields)

#===================================================================================================

def what_is(field_code):
    return FIELD_DICT.get(field_code, None)

def lookup_code(field_name):
    '''
    Search the FIELD_DICT with field_name as search term(s).
    Builds a list of tuples of a field name match and corresponding code.
    If only one match is found, just returns that tuple.
    '''
    search_results = []
    for k, v in FIELD_DICT.items():
        if reduce(lambda u, v: u and v, [term.upper() in v.upper() for term in field_name.split(' ')]):
            search_results.append((k,v))
    if len(search_results) == 1:
        return search_results[0]
    return search_results

def snake_to_pascal(string):
    return ' '.join(map(lambda word: word.upper().capitalize(), string.split('_')))

def function_to_pascalcase_str(func):
    return snake_to_pascal(func.__name__)

#===================================================================================================

def dangerous_line(line):
    redflags = ['import os', 'import shutil', 'from os import',
                'from shutil import', 'sleep(', 'import subprocess',
                'from subprocess import']
    return reduce(lambda x, y: x or y, [redflag in line for redflag in redflags])
