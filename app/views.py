# -*- coding: utf-8 -*-
"""
@author: jgoritski
"""
from flask import render_template, flash, redirect
from forms import LogicForm, VerificationForm, FPForm
from app import app, models, db
from utils import utils
from flask import Response
import os
from config import LOGIC_FUNCTION_BASEPATH
from flask.ext.login import login_user, login_required, current_user, logout_user

@app.route('/')
def index():
    form = VerificationForm()
    fpform = FPForm()
    return render_template('index.html', form=form, fpform=fpform)

@app.route('/api/action/<id>')
def get_action(id):
    action_id = str(id)
    action_ids = None
    if ',' in action_id:
        action_ids = list(set(action_id.split(',')))
    if not action_ids:
        data = utils.get_data(action_id).to_json()
    else:
        data = utils.get_data(action_ids).to_json()
    return Response(response=data, status=200, mimetype="application/json")

@app.route('/api/verify', methods=['GET', 'POST'])
def data_verification():
    form = VerificationForm()
    if form.validate_on_submit():
        try:
            check_ids = [int(i) for i in form.checks_to_run.data]
            action_id_data = map(lambda a: a.strip(), form.action_ids.data.replace('\r', '').split('\n'))
            action_ids = [action for action in action_id_data if action]
            print action_ids

            necessary_fields = set()
            logic_checks_to_run = [models.LogicCheck.query.get(i) for i in check_ids]
            for check in logic_checks_to_run:
                necessary_fields = necessary_fields.union(check.depends)

            verification_df = models.LogicCheck.verification(action_ids, fields=list(necessary_fields), checks=logic_checks_to_run)

            verification_df = models.FalsePositive.mark(verification_df)

            data = verification_df.to_json()
            return Response(response=data, status=200, mimetype="application/json")
        except Exception, e:
            print e
    return Response(response=None, status=403, mimetype="application/json")

@app.route('/logics')
def logics():
    all_checks = models.LogicCheck.query.all()
    return render_template('logics.html', all_checks=all_checks)

@app.route('/logic/new', methods=['GET', 'POST'])
@login_required
def new_logic():
    error = None
    form = LogicForm()
    field_dict = utils.FIELD_DICT
    if form.validate_on_submit():
        new_logic, exception = models.LogicCheck.generate_new_from_code(form.code_body.data)
        if new_logic:
            new_logic.error_message = form.error_message.data
            if form.name.data:
                new_logic.name = form.name.data
            db.session.add(new_logic)
            db.session.commit()
            return redirect('/logics')
        else:
            error = "Error encountered trying to save logic. Please check submission and try again."
    return render_template('new_logic.html', form=form, error=error, field_dict=field_dict)

@app.route('/logic/<id>/delete')
@login_required
def delete_logic(id):
    logic = models.LogicCheck.query.get(id)
    if logic:
        db.session.delete(logic)
        db.session.commit()
    return redirect('/logics')

@app.route('/help')
def help():
    return render_template('help.html')

@app.route('/api/fp/new', methods=['GET', 'POST'])
def new_fp():
    form = FPForm()
    if form.validate_on_submit():
        fp = models.FalsePositive(logic_id=form.logic_id.data, action_id=form.action_id.data, notes=form.notes.data)
        db.session.add(fp)
        db.session.commit()
        return Response(response="success", status=200, mimetype="application/json")
    else:
        print form.errors
    return Response(response=None, status=403, mimetype="application/json")

@app.route('/api/fp/<key>/delete')
def delete_fp(key):
    action_id, logic_id = key.split(',')
    fp = models.FalsePositive.query.filter_by(logic_id=logic_id, action_id=action_id).first()
    if fp:
        db.session.delete(fp)
        db.session.commit()
        return Response(response="success", status=200, mimetype="application/json")
    return Response(response=None, status=403, mimetype="application/json")

@app.route('/login')
def login():
    crow = models.User.query.filter_by(email="crow@bloomberg.net").first()
    login_user(crow)
    return redirect('/')

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect('/')
