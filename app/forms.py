# -*- coding: utf-8 -*-
"""
@author: jgoritski
"""
from flask.ext.wtf import Form
from wtforms import StringField, IntegerField, SelectMultipleField, TextAreaField, PasswordField
from wtforms.validators import DataRequired
from utils.utils import snake_to_pascal
from app import models

class LogicForm(Form):
    name = StringField('Name')
    error_message = StringField('Message')
    code_body = TextAreaField('Validation Code', validators=[DataRequired()])

def get_choices():
    return [(str(logic.id), logic.name) for logic in models.LogicCheck.query.all()]
class VerificationForm(Form):
    checks_to_run = SelectMultipleField('Checks to Run', validators=[DataRequired()])
    action_ids = TextAreaField('Action IDs', validators=[DataRequired()])

    def __init__(self):
        super(VerificationForm, self).__init__()
        self.checks_to_run.choices = get_choices()

class FPForm(Form):
    action_id = StringField('Action ID', validators=[DataRequired()])
    logic_id = IntegerField('Logic ID', validators=[DataRequired()])
    notes = TextAreaField('Notes')

class LoginForm(Form):
    email = StringField('Email', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])

