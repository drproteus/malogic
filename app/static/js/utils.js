/*
 * author: jgoritski@bloomberg.net
 *                                                   ___ 
 *                                                ,o88888 
 *                                             ,o8888888' 
 *                       ,:o:o:oooo.        ,8O88Pd8888" 
 *                   ,.::.::o:ooooOoOoO. ,oO8O8Pd888'" 
 *                 ,.:.::o:ooOoOoOO8O8OOo.8OOPd8O8O" 
 *                , ..:.::o:ooOoOOOO8OOOOo.FdO8O8" 
 *               , ..:.::o:ooOoOO8O888O8O,COCOO" 
 *              , . ..:.::o:ooOoOOOO8OOOOCOCO" 
 *               . ..:.::o:ooOoOoOO8O8OCCCC"o 
 *                  . ..:.::o:ooooOoCoCCC"o:o 
 *                  . ..:.::o:o:,cooooCo"oo:o: 
 *               `   . . ..:.:cocoooo"'o:o:::' 
 *               .`   . ..::ccccoc"'o:o:o:::' 
 *              :.:.    ,c:cccc"':.:.:.:.:.' 
 *            ..:.:"'`::::c:"'..:.:.:.:.:.' 
 *          ...:.'.:.::::"'    . . . . .' 
 *         .. . ....:."' `   .  . . '' 
 *       . . . ...."' 
 *       .. . ."'      
 *      . 
 *
 * 
 */
function ready(fn) {
  if (document.readyState != 'loading') {
    fn();
  } else {
    document.addEventListener('DOMContentLoaded', fn);
  }
}

function LogicResult(action_id, logic_check, result, error_message, logic_id, fp_notes) {
  var logic_result = {'action_id': action_id, 'logic_check': logic_check, 'result': result, 'logic_id': logic_id, 'error_message': error_message, 'fp_notes': fp_notes}
  return logic_result;
}

function parseDataJSON(data) {
  var pass = [];
  var fail = [];
  var fp = [];

  var record_count = Object.keys(data.ca_id).length;
  for (var i = 0; i < record_count; i++) {
    this_result = LogicResult(data.ca_id[i], data.logic_check[i], data.result[i], data.error_message[i], data.logic_id[i], data.fp_notes[i]);
    if (this_result.result) {
      pass = pass.concat(this_result);
    } else if (this_result.fp_notes != null) {
      fp = fp.concat(this_result);
    } else {
      fail = fail.concat(this_result);
    }
  }

  return {'pass': pass, 'fail': fail, 'fp': fp};
}
