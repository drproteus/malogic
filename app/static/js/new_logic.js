/*
 * author: jgoritski@bloomberg.net
 *                                                   ___ 
 *                                                ,o88888 
 *                                             ,o8888888' 
 *                       ,:o:o:oooo.        ,8O88Pd8888" 
 *                   ,.::.::o:ooooOoOoO. ,oO8O8Pd888'" 
 *                 ,.:.::o:ooOoOoOO8O8OOo.8OOPd8O8O" 
 *                , ..:.::o:ooOoOOOO8OOOOo.FdO8O8" 
 *               , ..:.::o:ooOoOO8O888O8O,COCOO" 
 *              , . ..:.::o:ooOoOOOO8OOOOCOCO" 
 *               . ..:.::o:ooOoOoOO8O8OCCCC"o 
 *                  . ..:.::o:ooooOoCoCCC"o:o 
 *                  . ..:.::o:o:,cooooCo"oo:o: 
 *               `   . . ..:.:cocoooo"'o:o:::' 
 *               .`   . ..::ccccoc"'o:o:o:::' 
 *              :.:.    ,c:cccc"':.:.:.:.:.' 
 *            ..:.:"'`::::c:"'..:.:.:.:.:.' 
 *          ...:.'.:.::::"'    . . . . .' 
 *         .. . ....:."' `   .  . . '' 
 *       . . . ...."' 
 *       .. . ."'      
 *      . 
 *
 * 
 */
function initEditor() {
  var editor = CodeMirror(document.getElementById('editor'), {
    mode: 'python',
    indentUnit: 4,
    value: 'import pandas as pd\ndef sample_function_name(ca_df):\n    return pd.Series(False, index=ca_df.index)'
  });
  editor.on('change', function(cm, change) {
    document.getElementById('code_body').innerHTML = cm.getValue();
  });
  if (document.getElementById('code_body').innerHTML !== "") {
      editor.setValue(document.getElementById('code_body').innerHTML);
  } else {
      document.getElementById('code_body').innerHTML = editor.getValue();
  }
}

ready(initEditor);
