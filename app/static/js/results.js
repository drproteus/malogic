/*
 * author: jgoritski@bloomberg.net
 *                                                   ___ 
 *                                                ,o88888 
 *                                             ,o8888888' 
 *                       ,:o:o:oooo.        ,8O88Pd8888" 
 *                   ,.::.::o:ooooOoOoO. ,oO8O8Pd888'" 
 *                 ,.:.::o:ooOoOoOO8O8OOo.8OOPd8O8O" 
 *                , ..:.::o:ooOoOOOO8OOOOo.FdO8O8" 
 *               , ..:.::o:ooOoOO8O888O8O,COCOO" 
 *              , . ..:.::o:ooOoOOOO8OOOOCOCO" 
 *               . ..:.::o:ooOoOoOO8O8OCCCC"o 
 *                  . ..:.::o:ooooOoCoCCC"o:o 
 *                  . ..:.::o:o:,cooooCo"oo:o: 
 *               `   . . ..:.:cocoooo"'o:o:::' 
 *               .`   . ..::ccccoc"'o:o:o:::' 
 *              :.:.    ,c:cccc"':.:.:.:.:.' 
 *            ..:.:"'`::::c:"'..:.:.:.:.:.' 
 *          ...:.'.:.::::"'    . . . . .' 
 *         .. . ....:."' `   .  . . '' 
 *       . . . ...."' 
 *       .. . ."'      
 *      . 
 *
 * 
 */
function makeResultsTable(parsedData) {
  var html = '<table class="table"><tr><th>Action ID</th><th>Logic Check</th><th>Result</th><th>Error Message</th><th>Logic ID</th><th>Notes</th></tr>';
  parsedData.fail.forEach(function(item) {
    if (item !== null) {
      var row = '<tr onclick="dataChange(this);" data-logic-id="'+item.logic_id+'" data-action-id="'+item.action_id+'" data-status="fail" class="data-row danger"><td>'+item.action_id+'</td><td>'+item.logic_check+'</td><td>'+item.result+'</td><td>'+item.error_message+'</td><td>'+item.logic_id+'</td><td></td></tr>';
      html += row;
    }
  });
  parsedData.fp.forEach(function(item) {
    if (item !== null) {
      var row = '<tr onclick="dataChange(this);" data-logic-id="'+item.logic_id+'" data-action-id="'+item.action_id+'" data-status="fp" class="data-row warning"><td>'+item.action_id+'</td><td>'+item.logic_check+'</td><td>'+item.result+'</td><td>'+item.error_message+'</td><td>'+item.logic_id+'</td><td>'+item.fp_notes+'</td></tr>';
      html += row;
    }
  });
  parsedData.pass.forEach(function(item) {
    if (item !== null) {
      var row = '<tr data-logic-id="'+item.logic_id+'" data-action-id="'+item.action_id+'" data-status="pass" class="data-row success"><td>'+item.action_id+'</td><td>'+item.logic_check+'</td><td>'+item.result+'</td><td></td><td>'+item.logic_id+'</td><td></td></tr>';
      html += row;
    }
  });
  html += '</table>';
  return html;
}

function updateTableRowStatus(action_id, logic_id, old_status, new_status, new_notes) {
  var newTableData = {'pass': tableData.pass, 'fail': [], 'fp': []};
  tableData.fail.forEach(function(item) {
    if (item.action_id == action_id && item.logic_id == logic_id) {
      var changedLogic = item;
      changedLogic.fp_notes = new_notes;
      newTableData.fp = newTableData.fp.concat(changedLogic);
    } else {
      newTableData.fail = newTableData.fail.concat(item);
    }
  });
  tableData.fp.forEach(function(item) {
    if (item.action_id == action_id && item.logic_id == logic_id) {
      var changedLogic = item;
      changedLogic.fp_notes = "";
      newTableData.fail = newTableData.fail.concat(changedLogic);
    } else {
      newTableData.fp = newTableData.fp.concat(item);
    }
  });
  tableData = newTableData;
  return newTableData;
}

function getFPFormData(logic_id, action_id, notes) {
  var formData = new FormData(document.getElementById('fp-form'));
  formData.set('notes', notes);
  formData.set('logic_id', logic_id);
  formData.set('action_id', action_id);
  return formData;
}

function dataChange(row) {
  var old_status = row.dataset.status;
  var logic_id = row.dataset.logicId;
  var action_id = row.dataset.actionId;
  if (old_status === 'fail') {
    var notes = prompt('Notes regarding this false positive');
    if (notes === null) {
      return;
    }
    var request = new XMLHttpRequest();
    request.open('POST', '/api/fp/new', true);
    request.onload = function() {
      if (request.status >= 200 && request.status < 400) {
        appendResults(updateTableRowStatus(action_id, logic_id, old_status, 'fp', notes));
      } else {
        // fail
      }
    };

    request.onerror = function() {};
    request.send(getFPFormData(logic_id, action_id, notes));
  } else if (old_status === 'fp') {
    if (!confirm('Delete this False Positive?')) {
      return;
    }
    var request = new XMLHttpRequest();
    request.open('GET', '/api/fp/'+action_id+','+logic_id+'/delete', true);
    request.onload = function() {
      if (request.status >= 200 && request.status < 400) {
        appendResults(updateTableRowStatus(action_id, logic_id, old_status, 'fail', null));
      } else {
        // fail
      }
    };
    request.onerror = function() {};
    request.send();
  }
}

function appendResults(data) {
  if (data.pass.length > 0 || data.fail.length > 0 || data.fp.length > 0) {
    document.querySelectorAll('.results').forEach(function(item) {
      item.style.display = 'block';
    });
    var targetDiv = document.querySelector('#results-target');
    targetDiv.innerHTML = makeResultsTable(data);
  } else {
    document.querySelectorAll('.results').forEach(function(item) {
      item.style.display = 'none';
    });
  }
}

function applyResults(data) {
  tableData = parseDataJSON(data);
  appendResults(tableData);
}
