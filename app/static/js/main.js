/*
 * author: jgoritski@bloomberg.net
 *                                                   ___ 
 *                                                ,o88888 
 *                                             ,o8888888' 
 *                       ,:o:o:oooo.        ,8O88Pd8888" 
 *                   ,.::.::o:ooooOoOoO. ,oO8O8Pd888'" 
 *                 ,.:.::o:ooOoOoOO8O8OOo.8OOPd8O8O" 
 *                , ..:.::o:ooOoOOOO8OOOOo.FdO8O8" 
 *               , ..:.::o:ooOoOO8O888O8O,COCOO" 
 *              , . ..:.::o:ooOoOOOO8OOOOCOCO" 
 *               . ..:.::o:ooOoOoOO8O8OCCCC"o 
 *                  . ..:.::o:ooooOoCoCCC"o:o 
 *                  . ..:.::o:o:,cooooCo"oo:o: 
 *               `   . . ..:.:cocoooo"'o:o:::' 
 *               .`   . ..::ccccoc"'o:o:o:::' 
 *              :.:.    ,c:cccc"':.:.:.:.:.' 
 *            ..:.:"'`::::c:"'..:.:.:.:.:.' 
 *          ...:.'.:.::::"'    . . . . .' 
 *         .. . ....:."' `   .  . . '' 
 *       . . . ...."' 
 *       .. . ."'      
 *      . 
 *
 * 
 */
function getVerification(formData, callback) {
  console.log(formData);
  var request = new XMLHttpRequest();
  request.open('POST', '/api/verify', true);

  request.onload = function() {
    if (request.status >= 200 && request.status < 400) {
      // Success!
      var data = JSON.parse(request.responseText);
      callback(data);
    } else {
      alert("An error has occurred! One or more of the selected logic functions may not be valid, please check and try again."); 
    }
  };

  request.onerror = function() {
    // There was a connection error of some sort
  };

  request.send(formData)
}

function main() {
  document.getElementById('inputs').onsubmit = function(e) {
    e.preventDefault();
    var f = e.target;
    var formData = new FormData(f);
    getVerification(formData, applyResults);
  };
}

ready(main);
