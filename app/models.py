# -*- coding: utf-8 -*-
"""
@author: jgoritski
"""
from app import db
from utils import utils
import numpy as np
import pandas as pd
from utils.utils import snake_to_pascal, dangerous_line
import os, re
from config import LOGIC_FUNCTION_MODULEBASE, LOGIC_FUNCTION_BASEPATH
from sqlalchemy import event
from utils.constants import EXPORT_FIELDS
from flask.ext.login import UserMixin
import bcrypt

class LogicCheck(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, index=True, unique=True)
    error_message = db.Column(db.String, index=True)
    field_dependencies = db.Column(db.String)
    function_name = db.Column(db.String)
    function_path = db.Column(db.String, unique=True)
    function_module = db.Column(db.String, unique=True)

    @property
    def logic_function(self):
        try:
            exec('from %s import %s as logic_function' % (self.function_module, self.function_name))
            return logic_function
        except Exception, e:
            print e 
            
    @property
    def filename(self):
        return self.function_path

    @property
    def depends(self):
        _s = set(self.field_dependencies.split(','))
        try:
            _s.remove('')
        except KeyError:
            pass
        return _s

    @property
    def implemented(self):
        if not self.logic_function:
            return False
        return True

    @property
    def function_body(self):
        lines = None
        with open(self.function_path) as f:
            lines = f.readlines()
        return lines

    @property
    def function_name_pretty(self):
        return self.name

    @classmethod
    def run_check(klass, check_id, ca_df):
        if type(check_id) == int:
            check = klass.query.get(check_id)
        else:
            check = check_id
        if not check:
            raise Exception, "Could not find specific Logic Check"
        temp_series = check.logic_function(ca_df)
        temp_series.name = 'result'
        temp_df = pd.DataFrame(data=None, index=ca_df.index,
                               columns=['logic_check',
                                        'logic_id',
                                        'result',
                                        'error_message'])
        temp_df.update(temp_series)
        temp_df['logic_check'] = check.name
        temp_df['error_message'] = check.error_message
        temp_df['ca_id'] = temp_df.index
        temp_df['logic_id'] = check.id
        temp_df.reset_index(drop=True, inplace=True)
        return temp_df

    @classmethod
    def verify_data(klass, ca_df, checks=None, mark_fp=True):
        if not checks:
            checks = klass.query.all()
        verification_df = pd.DataFrame(data=None, columns=['logic_check', 'logic_id', 'result', 'ca_id', 'fp_notes', 'error_message'])
        for check in checks:
            try:
                temp_df = verification_df.append(klass.run_check(check, ca_df), ignore_index=True)
            except AttributeError, e:
                print "[ERROR] Ran a check that depended on an attribute not specified in query (or unavailable), %s" % e
            else:
                verification_df = temp_df
        if mark_fp:
            verification_df = FalsePositive.mark(verification_df)
        return verification_df

    @classmethod
    def verification(klass, actions, fields=utils.TEST_FIELDS, checks=None):
        return klass.verify_data(utils.get_data(actions, fields), checks=checks)

    @classmethod
    def generate_new_from_code(klass, code_body):
        dependencies = set()
        function_name = None
        func_name_re = r'^def (\w+_*)\(\w+\)'
        try:
            for line in code_body.split('\n'):
                if dangerous_line(line):
                    raise Exception, "This code is probably dangerous."
                if not function_name:
                    _match = re.search(func_name_re, line)
                    if _match:
                        function_name = _match.group(1)
                for field in EXPORT_FIELDS:
                    if field in line:
                        dependencies.add(field)
        except Exception, e:
            print e
            return False, e
        if not function_name:
            raise Exception, "Failed to get a function name."
        # if function name, but no dependencies, add any field so we can run function.
        if not dependencies:
            dependencies.add(EXPORT_FIELDS[0])

        module_name = klass.get_unique_modulename(function_name) 
        path = os.path.join(LOGIC_FUNCTION_BASEPATH, module_name+'.py')
        module = "%s.%s" % (LOGIC_FUNCTION_MODULEBASE, module_name)

        name = klass.get_unique_name(snake_to_pascal(function_name))

        new_logic = klass(name=name,
                          function_name=function_name,
                          field_dependencies=','.join(list(dependencies)),
                          function_path=path,
                          function_module=module)
        try:
            with open(path, 'w') as f:
                for line in code_body:
                    f.write(line.replace('\r', ''))
        except Exception, e:
            return False, e
        else:
            db.session.add(new_logic)
            db.session.commit()
        return new_logic, None

    @classmethod
    def get_unique_modulename(klass, initial_modulename):
        module_name = initial_modulename
        i = 1
        while os.path.exists(os.path.join(LOGIC_FUNCTION_BASEPATH, module_name+'.py')):
            i += 1
            if i:
                module_name = initial_modulename+'_%s' % i
        return module_name

    @classmethod
    def get_unique_name(klass, initial_name):
        _name = initial_name
        name = _name
        i = 1
        while klass.query.filter_by(name=name).count() > 0:
            i += 1
            name = '%s %s' % (_name, i) 
        return name



def delete_logic_function_file(mapper, connection, target):
    try:
        os.remove(os.path.join(LOGIC_FUNCTION_BASEPATH, target.filename))
        os.remove(os.path.join(LOGIC_FUNCTION_BASEPATH, target.filename+"c"))
    except Exception, e:
        print e

event.listen(LogicCheck, 'after_delete', delete_logic_function_file)

#===================================================================================================

class FalsePositive(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    logic_id = db.Column(db.Integer, db.ForeignKey('logic_check.id'))
    action_id = db.Column(db.String)
    notes = db.Column(db.Text)

    @classmethod
    def check(klass, x):
        _q = FalsePositive.query.filter_by(logic_id=x.logic_id, action_id=x.ca_id)
        if _q.count() > 0:
            return _q.first().notes
        else:
            return np.nan
            
    @classmethod
    def mark(klass, verification_df):
        temp_df = verification_df[verification_df.result == False].apply(FalsePositive.check, axis = 1)
        temp_df.name = 'fp_notes'
        verification_df.update(temp_df)
        return verification_df

#===================================================================================================

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String, unique=True, index=True)
    password_hash = db.Column(db.String)
    is_admin = db.Column(db.Boolean)

    def is_password(self, password):
        return bcrypt.hashpw(password.encode('utf-8'), self.password_hash.encode('utf-8')) == self.password_hash

    @classmethod
    def register(klass, email, password):
        if klass.query.filter_by(email=email).count():
            return False
        if not "bloomberg.net" in email:
            return False
        password_hash = bcrypt.hashpw(password, bcrypt.gensalt(14))
        new_user = klass(email=email, password_hash=password_hash)
        db.session.add(new_user)
        db.session.commit()
        return True


