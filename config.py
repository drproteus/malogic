# -*- coding: utf-8 -*-
"""
@author: jgoritski
"""
import os
basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')
SQLALCHEMY_TRACK_MODIFICATIONS = True

WTF_CSRF_ENABLED = True
SECRET_KEY = 'not in the mountains not in a cave not in your mind can you hide from your death'

LOGIC_FUNCTION_MODULEBASE = "logic.logic_functions"
LOGIC_FUNCTION_BASEPATH = os.path.join(basedir, LOGIC_FUNCTION_MODULEBASE.replace('.', '/'))
