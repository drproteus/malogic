import os, shutil
from config import basedir, LOGIC_FUNCTION_BASEPATH
from app import models
from datetime import datetime

BACKUP_BASEPATH = os.path.join(basedir, 'logic')
def main():
    BACKUP_DIR = os.path.join(BACKUP_BASEPATH, "logic_functions_%s" % datetime.today().strftime("%Y%m%d-%H%M%S"))
    os.mkdir(BACKUP_DIR)
    for filename in [x for x in os.listdir(LOGIC_FUNCTION_BASEPATH) if ".pyc" not in x]:
        shutil.copy(os.path.join(LOGIC_FUNCTION_BASEPATH, filename), BACKUP_DIR)
    with open(os.path.join(BACKUP_DIR, 'info'), 'w') as f:
        for model in models.LogicCheck.query.all():
            f.write("$".join([model.name, model.function_name, model.function_path, model.function_module, model.field_dependencies, model.error_message])+'\n')
    print "backed up current functions to %s" % BACKUP_DIR

if __name__ == "__main__":
    main()

